# created by joecool890
# Extracts qualtrics survey into pandas
# Version 0.1.0

import numpy as np
import glob, os, time, getpass
import pandas as pd
import matplotlib.pyplot as plt

userName = getpass.getuser()
project_name = "tnt"
data_dir = "/Users/" + userName + "/PycharmProjects/" + project_name + "/data/qualtrics-survey.csv"

exclude = ["R_30o8v1ypvX7U31z", "R_1qe79ErWTb0DXRr"]

# function to determine confidence interval corrected for within-participants
def cm_standard_error(dataframe):
    # Get # of conditions (number of columns)
    num_conditions = dataframe.shape[1]

    # Morey portion of Cousineau-Morey method
    morey_correction = np.sqrt(float(num_conditions) / (num_conditions - 1))

    # Calculate mean of each participant and the grand mean
    parMean = dataframe.mean(axis=1)
    grandMean = parMean.mean()

    # Get standard error & add to original dataframe (subtract parMean, add grandMean)
    normMean = dataframe.sub(parMean, axis=0) + grandMean

    dataframe.loc["mean"] = normMean.mean()

    dataframe.loc["ste_c-m-corr"] = normMean.std() / np.sqrt(len(normMean) - 1)

    forexcel_dataframe = pd.concat([dataframe.loc["mean"], dataframe.loc["ste_c-m-corr"]], axis=1).unstack()
    return (forexcel_dataframe)

# Extract all data files into single np array
data_files      = glob.glob(data_dir)
raw_data = []
data = pd.read_csv(data_files[0], index_col="ResponseId", header=0)

raw_data.append(data)
df_raw = pd.concat(raw_data)

# Select only uniqueID + survey data
# as well as other cleaning methods
df_raw = df_raw.iloc[:,17:]
df_raw = df_raw.iloc[2:]
df_raw = df_raw.drop(exclude)

# remap string values into numbers for survey
remapping = {"1: Extremely unlikely": 1, "2: Moderately unlikely": 2, "3: Slightly unlikely": 3, "4: Slightly likely": 4,
              "5: Moderately likely": 5, "6: Extremely likely": 6,"1: Extremely unrelated": 1, "2: Moderately unrelated": 2,
              "3: Slightly unrelated": 3, "4: Slightly related": 4, "5: Moderately related": 5, "6: Extremely related": 6}
df = df_raw.replace(remapping)
df = df.reindex(sorted(df.columns), axis=1)

# Column Labels (not elegant I kno kno)
sim = ["neu-01","neu-02","neu-03","neu-04","neu-05","neu-06","neu-07","neu-08","neu-09","neu-10","tax-01","tax-02","tax-03",
       "tax-04","tax-05","tax-06","tax-07","tax-08","tax-09","tax-10","thm-01","thm-02","thm-03","thm-04","thm-05","thm-06",
       "thm-07","thm-08","thm-09","thm-10"]
co_occur = ["neu-01_c","neu-02_c","neu-03_c","neu-04a","neu-05_c","neu-06_c","neu-07_c","neu-08_c","neu-09_c","neu-10_c",
            "tax-01_c","tax-02_c","tax-03_c", "tax-04_c","tax-05_c","tax-06_c","tax-07_c","tax-08_c","tax-09_c","tax-10_c",
            "thm-01_c","thm-02_c","thm-03_c","thm-04_c","thm-05_c","thm-06_c","thm-07_c","thm-08_c","thm-09_c","thm-10_c"]

names = ["neu-01a","neu-02a","neu-03a","neu-04a","neu-05a","neu-06a","neu-07a","neu-08a","neu-09a","neu-10a",
            "tax-01a","tax-02a","tax-03a", "tax-04a","tax-05a","tax-06a","tax-07a","tax-08a","tax-09a","tax-10a",
            "thm-01a","thm-02a","thm-03a","thm-04a","thm-05a","thm-06a","thm-07a","thm-08a","thm-09a","thm-10a"]

# Get only co-occurrance data columns (" *_c")
label = [col for col in df.columns if '_cf' in col]

df_similar = df[sim]
df_cooccur = df[co_occur]
df_label = df[label]
df_names = df[names]

graph_RT = cm_standard_error(df_similar)
df_names.to_clipboard(excel=True, sep='\t')

# # Graph Data
# objects = [1,2,3,4,5,6,7,8,9,10]
# y_pos = np.arange(len(objects))
# fig, ax = plt.subplots(2,3, figsize=[10,5])
# ylim = (0,7)
# plt.setp(ax, ylim=ylim, xticks=[0,1,2,3,4,5,6,7,8,9], xticklabels=objects)
# print(y_pos)
# ax[0, 0].bar(y_pos, df_similar.iloc[:,0:10].mean(), yerr=df_similar.iloc[:,0:10].std()/np.sqrt(len(df)), align='center', color="red")
# ax[0, 0].set_ylabel("Similarity Rating")
# ax[0, 1].bar(y_pos, df_similar.iloc[:,10:20].mean(), yerr=df_similar.iloc[:,10:20].std()/np.sqrt(len(df)), align='center', color="#DAAA00")
# ax[0, 1].title.set_text('Similarity Data')
# ax[0, 2].bar(y_pos, df_similar.iloc[:,20:30].mean(), yerr=df_similar.iloc[:,20:30].std()/np.sqrt(len(df)), align='center', color="#002855")
#
# ax[1, 0].bar(y_pos, df_cooccur.iloc[:,0:10].mean(), yerr=df_cooccur.iloc[:,0:10].std()/np.sqrt(len(df)), align='center', color="red")
# ax[1, 0].set_ylabel("Co-occurance Rating")
# ax[1, 1].bar(y_pos, df_cooccur.iloc[:,10:20].mean(), yerr=df_cooccur.iloc[:,10:20].std()/np.sqrt(len(df)), align='center', color="#DAAA00")
# ax[1, 1].title.set_text('Co-occurance Data')
# ax[1, 2].bar(y_pos, df_cooccur.iloc[:,20:30].mean(), yerr=df_cooccur.iloc[:,20:30].std()/np.sqrt(len(df)), align='center', color="#002855")
# ax[1,0].set_xlabel("neutral pairs")
# ax[1,1].set_xlabel("taxonomic pairs")
# ax[1,2].set_xlabel("thematic pairs")
#
# fig.show()