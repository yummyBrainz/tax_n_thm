# Creates randomized CB matrix for each participant
# ver 1.0.0 = iter01 and iter02s
# current version: 3.0.0 iter05 (two objects)


import numpy as np
import pandas as pd
import random


def cb_matrix(file, rep_num, iter):

    # Read in file
    counter_balance_csv = pd.read_csv(file)
    counter_balance = []

    if iter < 3:

        for rep in range(rep_num):
            # Designate cued object location
            cued_object_loc_hori = [1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4]
            cued_object_loc_vert = [1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4]

            # Shuffle cued object locations
            np.random.shuffle(cued_object_loc_hori)
            np.random.shuffle(cued_object_loc_vert)

            # All shuffled cued object location
            cued_object_loc = cued_object_loc_hori + cued_object_loc_vert

            # Randomly assign cued object location
            counter_balance_csv["cued-object_loc"] = cued_object_loc

            # Shuffle
            counter_balance_csv_shf = counter_balance_csv.sample(frac=1)

            # Designate object pair to counterbalance matrix1
            counter_balance_csv_shf["obj-pair_num"] = rep

            # Designate semantic relationship condition
            counter_balance_csv_shf["sem-rel"] = np.where(counter_balance_csv_shf["obj-pair_num"] <= 6, "1_thm", "2_tax")

            # Assign which block each trial belongs to
            counter_balance_csv_shf["block_num"] = np.arange(len(counter_balance_csv_shf))

            # Combine
            counter_balance.append(counter_balance_csv_shf)

        # Concat into DF
        counter_balance_df = pd.concat(counter_balance)

        # reset index
        counter_balance_df = counter_balance_df.reset_index(drop=True)

        # Sort by block number
        counter_balance_df = counter_balance_df.sort_values(by=['block_num'])

        # Add trial number
        counter_balance_df["trial_num"] = np.arange(len(counter_balance_df)) + 1
        # Conditions for location of target object
        # Obj-orientation =  1: horizontal; 2: vertical
        # Condition = 1: valid; 2: within-pair, 3: between-pair
        target_object_conditions = [
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 1) & (
                    counter_balance_df["condition"] == 1),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 1) & (
                    counter_balance_df["condition"] == 2),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 1) & (
                    counter_balance_df["condition"] == 3),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 2) & (
                    counter_balance_df["condition"] == 1),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 2) & (
                    counter_balance_df["condition"] == 2),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 2) & (
                    counter_balance_df["condition"] == 3),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 3) & (
                    counter_balance_df["condition"] == 1),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 3) & (
                    counter_balance_df["condition"] == 2),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 3) & (
                    counter_balance_df["condition"] == 3),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 4) & (
                    counter_balance_df["condition"] == 1),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 4) & (
                    counter_balance_df["condition"] == 2),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 4) & (
                    counter_balance_df["condition"] == 3),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 1) & (
                    counter_balance_df["condition"] == 1),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 1) & (
                    counter_balance_df["condition"] == 2),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 1) & (
                    counter_balance_df["condition"] == 3),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 2) & (
                    counter_balance_df["condition"] == 1),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 2) & (
                    counter_balance_df["condition"] == 2),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 2) & (
                    counter_balance_df["condition"] == 3),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 3) & (
                    counter_balance_df["condition"] == 1),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 3) & (
                    counter_balance_df["condition"] == 2),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 3) & (
                    counter_balance_df["condition"] == 3),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 4) & (
                    counter_balance_df["condition"] == 1),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 4) & (
                    counter_balance_df["condition"] == 2),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 4) & (
                    counter_balance_df["condition"] == 3)
        ]

        target_object = [1, 2, 3, 2, 1, 4, 3, 4, 1, 4, 3, 2, 1, 3, 2, 2, 4, 1, 3, 1, 4, 4, 2, 3]

        # Concat target_object to DF
        counter_balance_df["target_object_loc"] = np.select(target_object_conditions, target_object)

        # Stupid inefficient way to figure out object locations for the semantically related (THM or TAX)
        sem_object_location_conditions = [
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 1) & (
                    counter_balance_df["cued-object_loc"] == 1),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 1) & (
                    counter_balance_df["cued-object_loc"] == 2),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 1) & (
                    counter_balance_df["cued-object_loc"] == 3),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 1) & (
                    counter_balance_df["cued-object_loc"] == 4),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 2) & (
                    counter_balance_df["cued-object_loc"] == 1),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 2) & (
                    counter_balance_df["cued-object_loc"] == 2),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 2) & (
                    counter_balance_df["cued-object_loc"] == 3),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 2) & (
                    counter_balance_df["cued-object_loc"] == 4),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 1) & (
                    counter_balance_df["cued-object_loc"] == 1),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 1) & (
                    counter_balance_df["cued-object_loc"] == 2),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 1) & (
                    counter_balance_df["cued-object_loc"] == 3),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 1) & (
                    counter_balance_df["cued-object_loc"] == 4),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 2) & (
                    counter_balance_df["cued-object_loc"] == 1),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 2) & (
                    counter_balance_df["cued-object_loc"] == 2),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 2) & (
                    counter_balance_df["cued-object_loc"] == 3),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 2) & (
                    counter_balance_df["cued-object_loc"] == 4)
        ]

        stim1_pos = [1, 2, 3, 4, 2, 1, 4, 3, 1, 2, 3, 4, 3, 4, 1, 2]
        stim2_pos = [2, 1, 4, 3, 1, 2, 3, 4, 3, 4, 1, 2, 1, 2, 3, 4]

        counter_balance_df["stim1_pos"] = np.select(sem_object_location_conditions, stim1_pos, default=999)
        counter_balance_df["stim2_pos"] = np.select(sem_object_location_conditions, stim2_pos, default=999)

        # Stupid inefficient way to figure out object locations for the neutral objects
        non_sem_object_location_conditions = [
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 3) & (
                    counter_balance_df["cued-object_loc"] == 1),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 3) & (
                    counter_balance_df["cued-object_loc"] == 2),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 3) & (
                    counter_balance_df["cued-object_loc"] == 3),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 3) & (
                    counter_balance_df["cued-object_loc"] == 4),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 4) & (
                    counter_balance_df["cued-object_loc"] == 1),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 4) & (
                    counter_balance_df["cued-object_loc"] == 2),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 4) & (
                    counter_balance_df["cued-object_loc"] == 3),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 4) & (
                    counter_balance_df["cued-object_loc"] == 4),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 3) & (
                    counter_balance_df["cued-object_loc"] == 1),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 3) & (
                    counter_balance_df["cued-object_loc"] == 2),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 3) & (
                    counter_balance_df["cued-object_loc"] == 3),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 3) & (
                    counter_balance_df["cued-object_loc"] == 4),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 4) & (
                    counter_balance_df["cued-object_loc"] == 1),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 4) & (
                    counter_balance_df["cued-object_loc"] == 2),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 4) & (
                    counter_balance_df["cued-object_loc"] == 3),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 4) & (
                    counter_balance_df["cued-object_loc"] == 4)
        ]

        stim3_pos = [1, 2, 3, 4, 2, 1, 4, 3, 1, 2, 3, 4, 3, 4, 1, 2]
        stim4_pos = [2, 1, 4, 3, 1, 2, 3, 4, 3, 4, 1, 2, 1, 2, 3, 4]

        counter_balance_df["stim3_pos"] = np.select(non_sem_object_location_conditions, stim3_pos, default=999)
        counter_balance_df["stim4_pos"] = np.select(non_sem_object_location_conditions, stim4_pos, default=999)
        counter_balance_df["target_randomization"] = np.random.rand(len(counter_balance_df), 1)

        return counter_balance_df

    elif iter == 3:

        for rep in range(rep_num):

            # initialize cued_loc_final array
            cued_loc_final = []

            # Repeat for number of total orientation
            for i in range(2):

                # cued locations
                cued_object_loc_1 = [1, 2, 3, 4]
                cued_object_loc_2 = [1, 2, 3, 4]
                cued_object_loc_3 = [1, 2, 3, 4]

                # shuffle cued location
                np.random.shuffle(cued_object_loc_1)
                np.random.shuffle(cued_object_loc_2)
                np.random.shuffle(cued_object_loc_3)

                # combine all this inefficient shit together
                cued_object_loc_prelim = np.concatenate((cued_object_loc_1, cued_object_loc_2))
                cued_object_loc = np.append(cued_object_loc_prelim,cued_object_loc_3[0])
                cued_loc_final = np.append(cued_loc_final, cued_object_loc).astype(int)

            # Randomly assign cued object location
            counter_balance_csv["cued-object_loc"] = cued_loc_final

            # Shuffle counterbalance
            counter_balance_csv_shf = counter_balance_csv.sample(frac=1)

            # Designate object pair to counterbalance matrix1
            counter_balance_csv_shf["obj-pair_num"] = rep

            # Assign which block each trial belongs to
            counter_balance_csv_shf["block_num"] = np.arange(len(counter_balance_csv_shf))

            # randomize trial
            trial_randomizer = np.arange(len(counter_balance_csv_shf))
            np.random.shuffle(trial_randomizer)
            counter_balance_csv_shf["trial_randomizer"] = trial_randomizer

            # Combine
            counter_balance.append(counter_balance_csv_shf)

        # Concat into DF
        counter_balance_df = pd.concat(counter_balance)

        # reset index
        counter_balance_df = counter_balance_df.reset_index(drop=True)

        # Sort by block number
        counter_balance_df = counter_balance_df.sort_values(by=["block_num", "trial_randomizer"], ascending=(True, True))

        # Add trial number
        counter_balance_df["trial_num"] = np.arange(len(counter_balance_df)) + 1

        # Add trial type (to differentiate between fixation task)
        counter_balance_df["trial_type"] = 1

        # randomize 3 or 4th object
        counter_balance_df["scramble_randomization"] = np.random.rand(len(counter_balance_df), 1)
        counter_balance_df["cued-object"] = np.where((counter_balance_df["scramble_randomization"] < .5) & (counter_balance_df["cued-object"] >= 3), 4, counter_balance_df["cued-object"])

        # randomize target location
        counter_balance_df["target_randomization"] = np.random.rand(len(counter_balance_df), 1)

        # Designate semantic relationship condition
        sem_conditions = [
            (counter_balance_df["obj-pair_num"] <= 4),
            (counter_balance_df["obj-pair_num"] > 4) & (counter_balance_df["obj-pair_num"] <= 9),
            (counter_balance_df["obj-pair_num"] > 9) & (counter_balance_df["obj-pair_num"] <= 14)
        ]

        sem_pairs = ["2_tax", "1_thm", "3_neu"]
        counter_balance_df["sem-rel"] = np.select(sem_conditions, sem_pairs)

        # Conditions for location of target object
        # Obj-orientation =  1: horizontal; 2: vertical
        # Condition = 1: valid; 2: within-pair, 3: between-pair
        target_object_conditions = [
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 1) & (
                    counter_balance_df["condition"] == 1),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 1) & (
                    counter_balance_df["condition"] == 2),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 1) & (
                    counter_balance_df["condition"] == 3),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 2) & (
                    counter_balance_df["condition"] == 1),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 2) & (
                    counter_balance_df["condition"] == 2),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 2) & (
                    counter_balance_df["condition"] == 3),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 3) & (
                    counter_balance_df["condition"] == 1),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 3) & (
                    counter_balance_df["condition"] == 2),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 3) & (
                    counter_balance_df["condition"] == 3),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 4) & (
                    counter_balance_df["condition"] == 1),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 4) & (
                    counter_balance_df["condition"] == 2),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object_loc"] == 4) & (
                    counter_balance_df["condition"] == 3),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 1) & (
                    counter_balance_df["condition"] == 1),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 1) & (
                    counter_balance_df["condition"] == 2),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 1) & (
                    counter_balance_df["condition"] == 3),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 2) & (
                    counter_balance_df["condition"] == 1),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 2) & (
                    counter_balance_df["condition"] == 2),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 2) & (
                    counter_balance_df["condition"] == 3),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 3) & (
                    counter_balance_df["condition"] == 1),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 3) & (
                    counter_balance_df["condition"] == 2),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 3) & (
                    counter_balance_df["condition"] == 3),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 4) & (
                    counter_balance_df["condition"] == 1),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 4) & (
                    counter_balance_df["condition"] == 2),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object_loc"] == 4) & (
                    counter_balance_df["condition"] == 3)
        ]

        target_object = [1, 2, 3, 2, 1, 4, 3, 4, 1, 4, 3, 2, 1, 3, 2, 2, 4, 1, 3, 1, 4, 4, 2, 3]

        # Concat target_object to DF
        counter_balance_df["target_object_loc"] = np.select(target_object_conditions, target_object)

        # Stupid inefficient way to figure out object locations for the semantically related (THM or TAX)
        sem_object_location_conditions = [
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 1) & (
                    counter_balance_df["cued-object_loc"] == 1),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 1) & (
                    counter_balance_df["cued-object_loc"] == 2),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 1) & (
                    counter_balance_df["cued-object_loc"] == 3),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 1) & (
                    counter_balance_df["cued-object_loc"] == 4),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 2) & (
                    counter_balance_df["cued-object_loc"] == 1),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 2) & (
                    counter_balance_df["cued-object_loc"] == 2),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 2) & (
                    counter_balance_df["cued-object_loc"] == 3),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 2) & (
                    counter_balance_df["cued-object_loc"] == 4),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 1) & (
                    counter_balance_df["cued-object_loc"] == 1),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 1) & (
                    counter_balance_df["cued-object_loc"] == 2),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 1) & (
                    counter_balance_df["cued-object_loc"] == 3),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 1) & (
                    counter_balance_df["cued-object_loc"] == 4),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 2) & (
                    counter_balance_df["cued-object_loc"] == 1),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 2) & (
                    counter_balance_df["cued-object_loc"] == 2),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 2) & (
                    counter_balance_df["cued-object_loc"] == 3),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 2) & (
                    counter_balance_df["cued-object_loc"] == 4)
        ]

        stim1_pos = [1, 2, 3, 4, 2, 1, 4, 3, 1, 2, 3, 4, 3, 4, 1, 2]
        stim2_pos = [2, 1, 4, 3, 1, 2, 3, 4, 3, 4, 1, 2, 1, 2, 3, 4]

        counter_balance_df["stim1_pos"] = np.select(sem_object_location_conditions, stim1_pos, default=999)
        counter_balance_df["stim2_pos"] = np.select(sem_object_location_conditions, stim2_pos, default=999)

        # Stupid inefficient way to figure out object locations for the neutral objects
        non_sem_object_location_conditions = [
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 3) & (
                    counter_balance_df["cued-object_loc"] == 1),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 3) & (
                    counter_balance_df["cued-object_loc"] == 2),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 3) & (
                    counter_balance_df["cued-object_loc"] == 3),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 3) & (
                    counter_balance_df["cued-object_loc"] == 4),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 4) & (
                    counter_balance_df["cued-object_loc"] == 1),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 4) & (
                    counter_balance_df["cued-object_loc"] == 2),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 4) & (
                    counter_balance_df["cued-object_loc"] == 3),
            (counter_balance_df["obj-orientation"] == 1) & (counter_balance_df["cued-object"] == 4) & (
                    counter_balance_df["cued-object_loc"] == 4),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 3) & (
                    counter_balance_df["cued-object_loc"] == 1),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 3) & (
                    counter_balance_df["cued-object_loc"] == 2),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 3) & (
                    counter_balance_df["cued-object_loc"] == 3),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 3) & (
                    counter_balance_df["cued-object_loc"] == 4),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 4) & (
                    counter_balance_df["cued-object_loc"] == 1),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 4) & (
                    counter_balance_df["cued-object_loc"] == 2),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 4) & (
                    counter_balance_df["cued-object_loc"] == 3),
            (counter_balance_df["obj-orientation"] == 2) & (counter_balance_df["cued-object"] == 4) & (
                    counter_balance_df["cued-object_loc"] == 4)
        ]

        stim3_pos = [1, 2, 3, 4, 2, 1, 4, 3, 1, 2, 3, 4, 3, 4, 1, 2]
        stim4_pos = [2, 1, 4, 3, 1, 2, 3, 4, 3, 4, 1, 2, 1, 2, 3, 4]

        counter_balance_df["stim3_pos"] = np.select(non_sem_object_location_conditions, stim3_pos, default=999)
        counter_balance_df["stim4_pos"] = np.select(non_sem_object_location_conditions, stim4_pos, default=999)

        # No longer needed in the end
        counter_balance_df = counter_balance_df.drop(columns=["trial_randomizer", "scramble_randomization"])

        return counter_balance_df


def cb_matrix_two_obj(file, practice=0):

    counter_balance_csv = pd.read_csv(file)

    if practice == 1:
        cued_object = []
        for i in range(len(counter_balance_csv)):
            cued_object_select = random.random()
            if cued_object_select < .5:
                cued_object.append("stim1")
            else:
                cued_object.append("stim2")

        counter_balance_csv["cued_object"] = cued_object

    else:
        pass

    # Randomize target side
    target_side_list = []
    for i in range(len(counter_balance_csv)):
        target_side = random.random()
        if target_side < .5:
            target_side_list.append("target_right")
        else:
            target_side_list.append("target_left")

    # Randomize object side
    stim1_loc_list = []
    stim2_loc_list = []
    for i in range(len(counter_balance_csv)):
        object_loc = random.random()
        if object_loc < .5:
            stim1_loc_list.append("left")
            stim2_loc_list.append("right")
        else:
            stim1_loc_list.append("right")
            stim2_loc_list.append("left")

    # append newly added variables to counterbalance file
    counter_balance_csv["target_side"] = target_side_list
    counter_balance_csv["stim1_loc"] = stim1_loc_list
    counter_balance_csv["stim2_loc"] = stim2_loc_list

    # Shuffle
    counter_balance_csv = counter_balance_csv.sample(frac=1)

    # reset index
    counter_balance_csv = counter_balance_csv.reset_index(drop=True)

    return counter_balance_csv


cb_matrix_two_obj("balanceFactors_spa-att_iter05_prac.csv", practice=1)
