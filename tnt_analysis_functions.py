


# Separate out neutral trials

def sem_conds(df):
    if df["sem_conditions"] == "1_SR" and df["sem-rel"] == "1_thm":
        return "1_thm"
    elif df["sem_conditions"] == "1_SR" and df["sem-rel"] == "2_tax":
        return "2_tax"
    else:
        return "3_neu"



# Separate the neutral trials


def neutral(df):
    if df["sem_conditions"] == "2_NEU" and df["sem-rel"] == "1_thm":
        return "1_neu_in_thm"
    elif df["sem_conditions"] == "2_NEU" and df["sem-rel"] == "2_tax":
        return "2_neu_in_tax"
    else:
        return 0




def canon_orient(df):
    if df["obj-pair_num"] == 0:
        return 1
    elif df["obj-pair_num"] == 1:
        return 1
    elif df["obj-pair_num"] == 2:
        return 1
    elif df["obj-pair_num"] == 3:
        return 1
    elif df["obj-pair_num"] == 4:
        return 1
    elif df["obj-pair_num"] == 5:
        return 1
    elif df["obj-pair_num"] == 6:
        return 1
    elif df["obj-pair_num"] == 7:
        return 1
    elif df["obj-pair_num"] == 8:
        return 1
    elif df["obj-pair_num"] == 9:
        return 1
    elif df["obj-pair_num"] == 10:
        return 1
    elif df["obj-pair_num"] == 11:
        return 1
    elif df["obj-pair_num"] == 12:
        return 1
    elif df["obj-pair_num"] == 13:
        return 1


def halves(df):
    if df["trial_num"] <= 168:
        return "1st_half"
    else:
        return "2nd half"