import pandas as pd

class exp_var():
    def __init__(self):
        self.ITI = 500
        self.ISI = 100
        self.pres_time = 250
        self.cue_time = 100
        self.subject_group = 10




def insert_essential_columns():
    essential_df = pd.DataFrame(columns=[
        "type", "content", "trialText", "stim1", "stim2", "stim3", "stim4", "stim5", "stim6", "stimPos",
        "stimFormat", "ITI", "presTime", "keyboard", "key", "feedback", "feedbackTime", "feedbackOptions",
        "responseType", "responseOptions", "head", "button1", "block_num", "trial_num", "condition", "sem-rel",
        "cued-object", "cued-object_loc", "target_object_loc", "obj-orientation", "obj-pair_num",
        "stim1_pos", "stim2_pos", "stim3_pos", "stim4_pos", "target_kind", "trial_type", "required"
    ]
    )
    return essential_df


