# Creates instructions and break portion of testable experiment
import pandas as pd
import numpy as np


def insert_instructions():

    ### INSTRUCTIONS ###
    begin_message = "Click the button below for instructions"
    race_question = "American Indian or Alaska Native;Asian;Black or African American;Native Hawaiian or Other Pacific Islander; White;More than one;Not Listed"
    gender_question = "Male; Female; Non-Binary; Decline to State"
    instructions_np = np.array([
        ["form", "", "", "", "", "", "", "", "dropdown", gender_question, "What is your gender identity?", 1],
        ["form", "", "", "", "", "", "", "", "dropdown", race_question, "With which race do you identify?", 1],
        ["form", "", "", "", "", "", "", "", "dropdown", "yes; no", "Do you identify as Hispanic", 1],
        ["instructions", begin_message, "", "", "Next", "", "", "", "", "", "", ""],
        ["instructions", "", "instructions_01", ".png", "Next", "", "", "", "", "", "", ""],
        ["instructions", "", "instructions_02", ".png", "Next", "", "", "", "", "", "", ""],
        ["instructions", "", "instructions_03", ".png", "Next", "", "", "", "", "", "", ""],
        ["instructions", "", "instructions_04", ".png", "Begin", "", "", "", "", "", "", ""],
        ["learn", "", "blank", ".png", "", 2000, "allTrial", "50 50", "", "", "", ""]
    ])

    instructions_df = pd.DataFrame(data=instructions_np, columns=["type", "content", "stim1", "stimFormat", "button1",
                                                                    "presTime", "fixation", "stimPos", "responseType",
                                                                    "responseOptions", "head", "required"])
    return instructions_df


def insert_end_practice():

    practice_message = "You have finished the practice. Click the button below to start the experiment"

    instructions_2_np = np.array([
        ["instructions", practice_message, "", "", "Next", "", "", "", "", "", "", ""],
        ["learn", "", "blank", ".png", "", 2000, "allTrial", "50 50", "", "", "", ""]
    ])

    instructions_2_df = pd.DataFrame(data=instructions_2_np, columns=["type", "content", "stim1", "stimFormat", "button1",
                                                                    "presTime", "fixation", "stimPos", "responseType",
                                                                    "responseOptions", "head", "required"])

    return instructions_2_df
