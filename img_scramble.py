import cv2
import numpy as np
import glob

stim_name = "./stim/v2/staircase/*.png"
stim_files = glob.glob(stim_name)
stim_list = list(range(len(stim_files)))

for stim in stim_list:

    #Get image name
    stim_name = stim_files[stim]

    # Read in image
    img = cv2.imread(stim_name, -1)

    # Get image shape
    width, height = img.shape[:2]

    # convert image to numpy array
    img_np = np.array(img)

    # initialize color array
    colors = []

    # Define the size of pixel block
    block_size = 50

    # Go through image pixel block by pixel block and extract color info (BGRA)
    for w in range(width // block_size):
        for h in range(height // block_size):
            if np.average(img_np[w * block_size:w * block_size + block_size, h * block_size:h * block_size + block_size,
                          3]) != 0:
                pixel_color = img_np[w * block_size:w * block_size + block_size,
                              h * block_size:h * block_size + block_size, 0:4]
                colors.append(pixel_color)

    # Store to colors array
    colors = np.array(colors)

    # now give it a nice shake
    np.random.shuffle(colors)

    # time to put back the shuffled pixel block
    color_counter = 0
    for w in range(width // block_size):
        for h in range(height // block_size):
            if np.average(img_np[w * block_size:w * block_size + block_size, h * block_size:h * block_size + block_size,
                          3]) != 0:
                img_np[w * block_size:w * block_size + block_size, h * block_size:h * block_size + block_size, 0:4] = \
                colors[color_counter]
                color_counter += 1

    # save as
    # new_name = stim_name.replace(".png", "_non.png")
    cv2.imwrite(stim_name, img_np)

print("all shuffled")
