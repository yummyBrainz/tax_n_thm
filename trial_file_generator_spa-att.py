import numpy as np
import pandas as pd
import random
import create_cb_matrix
import instructions_testable
from testable import exp_var
import testable

# version 3.0: for iter05 (2 objects)

# Variables
ITI = 500
ISI_time = 100
object_preview_duration = 750
cue_time = 100
target_time = 200
fovea = "0, 0"

fixation = "fixation_black"

# instructions + break
instructions_df = instructions_testable.insert_instructions()
end_practice_break = instructions_testable.insert_end_practice()

rght_coor = 180
left_coor = - rght_coor
# position
object_left_pos = str(left_coor) + ", 0"
object_rght_pos = str(rght_coor) + ", 0"
cue_left_pos = str(left_coor) + ", 0"
cue_right_pos = str(rght_coor) + ", 0"

str_space = "; "
pos_starter = "'"

subjectGroup = 10

# object list (will be shuffled)
all_objects = np.array([
    ["neu_01a_truck", "neu_01b_gondola"],
    ["neu_02a_bee", "neu_02b_eagle"],
    ["neu_03a_hoover", "neu_03b_mower"],
    ["neu_04a_outlet", "neu_04b_keyhole"],
    ["neu_05a_bat", "neu_05b_airplane"],
    ["neu_06a_keg", "neu_06b_sack"],
    ["neu_07a_glasses", "neu_07b_camera"],
    ["neu_08a_strawberry", "neu_08b_gum"],
    ["neu_09a_laundry", "neu_09b_dishes"],
    ["neu_10a_bbat", "neu_10b_flippers"],
    ["tax_01a_train", "tax_01b_boat"],
    ["tax_02a_helmet", "tax_02b_crown"],
    ["tax_03a_soccer", "tax_03b_football"],
    ["tax_04a_car", "tax_04b_tractor"],
    ["tax_05a_panda", "tax_05b_grizzly"],
    ["tax_06a_goldfish", "tax_06b_haddock"],
    ["tax_07a_apple", "tax_07b_banana"],
    ["tax_08a_doctor", "tax_08b_police"],
    ["tax_09a_owl", "tax_09b_sparrow"],
    ["tax_10a_rat", "tax_10b_otter"],
    ["thm_01a_saw", "thm_01b_tree"],
    ["thm_02a_umbrella", "thm_02b_rainboots"],
    ["thm_03a_burger", "thm_03b_ketchup"],
    ["thm_04a_rub-duck", "thm_04b_tub"],
    ["thm_05a_chicken", "thm_05b_egg"],
    ["thm_06a_sushi", "thm_06b_soysauce"],
    ["thm_07a_armor", "thm_07b_blade"],
    ["thm_08a_cake", "thm_08b_candles"],
    ["thm_09a_pot", "thm_09b_plant"],
    ["thm_10a_corsage", "thm_10b_gown"]
]
)

all_objects = pd.DataFrame(data=all_objects, columns=["stim1", "stim2"])

# Load objects for practice
prac_objects = np.array([
    ["1a", "1b"],
    ["2a", "2b"],
    ["3a", "3b"],
    ["4a", "4b"],
    ["5a", "5b"],
    ["6a", "6b"],
    ["7a", "7b"],
    ["8a", "8b"],
    ["9a", "9b"],
    ["10a", "10b"],
    ["11a", "11b"],
    ["12a", "12b"],
    ["13a", "13b"],
    ["14a", "14b"],
    ["15a", "15b"]
])

practice_objects = pd.DataFrame(data=prac_objects, columns=["stim1", "stim2"])

# # initialize dataframes
all_trials = pd.DataFrame([])
practice_trials = pd.DataFrame([])

# load counter balance for practice (24 trials)
prac_trial_cb = create_cb_matrix.cb_matrix_two_obj("balanceFactors_spa-att_iter05_prac.csv", practice=1)

# replace object pairs with practice objects
prac_trial_cb["obj-pair_num"] = pd.DataFrame(np.random.randint(0, 14, size=(24, 1)))

# initialize counters

for p in range(len(prac_trial_cb)):
    # print("prac trial # " + str(p))
    # print( "catch index" + str(prac_catch_trial_index))

    # initialize main dataframe
    final_trial = pd.DataFrame([])

    # START TRIAL (0/4) - ITI and alert for fixation prior to beginning of trial
    start_trial = testable.insert_essential_columns()
    start_trial["type"] = ["learn"]
    start_trial["stim1"] = fixation
    start_trial["stimFormat"] = ".png"
    start_trial["presTime"] = 700
    start_trial["ITI"] = ITI
    start_trial["stimPos"] = fovea

    # OBJECTS (1/5)
    objects = testable.insert_essential_columns()
    object_counter = prac_trial_cb["object_pair"][p] - 1 + 5 * (prac_trial_cb["sem_rel"][p] - 1)
    objects["type"] = ["learn"]
    objects["stim1"] = practice_objects["stim1"][object_counter]
    objects["stim2"] = practice_objects["stim2"][object_counter]
    objects["stim3"] = fixation
    objects["stimFormat"] = ".png"
    objects["presTime"] = object_preview_duration

    if prac_trial_cb["stim1_loc"][p] == "left":
        objects["stimPos"] = object_left_pos + str_space + object_rght_pos + str_space + fovea

    elif prac_trial_cb["stim1_loc"][p] == "right":
        objects["stimPos"] = object_rght_pos + str_space + object_left_pos + str_space + fovea

    # CUE (2/4)
    cue = objects.copy()
    if prac_trial_cb["cued_object"][p] == "stim1" and prac_trial_cb["stim1_loc"][p] == "left":
        cue["stim4"] = "cue_left"
        cue[
            "stimPos"] = object_left_pos + str_space + object_rght_pos + str_space + fovea + str_space + cue_left_pos
    elif prac_trial_cb["cued_object"][p] == "stim1" and prac_trial_cb["stim1_loc"][p] == "right":
        cue["stim4"] = "cue_right"
        cue[
            "stimPos"] = object_rght_pos + str_space + object_left_pos + str_space + fovea + str_space + cue_right_pos
    elif prac_trial_cb["cued_object"][p] == "stim2" and prac_trial_cb["stim1_loc"][p] == "left":
        cue["stim4"] = "cue_right"
        cue[
            "stimPos"] = object_left_pos + str_space + object_rght_pos + str_space + fovea + str_space + cue_right_pos
    elif prac_trial_cb["cued_object"][p] == "stim2" and prac_trial_cb["stim1_loc"][p] == "right":
        cue["stim4"] = "cue_left"
        cue[
            "stimPos"] = object_rght_pos + str_space + object_left_pos + str_space + fovea + str_space + cue_left_pos

    cue["presTime"] = cue_time

    # ISI (3/5)
    ISI = objects.copy()
    ISI["presTime"] = ISI_time

    # TARGET (4/5)
    target = objects.copy()
    target["stim4"] = prac_trial_cb["target_side"][p]
    target["stim5"] = "non_target"

    if prac_trial_cb["target_side"][p] == "target_left":
        target["stimPos"] = objects["stimPos"] + str_space + object_left_pos + str_space + object_rght_pos
    elif prac_trial_cb["target_side"][p] == "target_right":
        target["stimPos"] = objects["stimPos"] + str_space + object_rght_pos + str_space + object_left_pos
    target["presTime"] = target_time

    # TEST (5/5)
    test = start_trial.copy()
    test["ITI"] = 0
    test["type"] = "test"
    test["keyboard"] = "f j"

    if prac_trial_cb["target_side"][p] == "target_left":
        test["key"] = "f"
    else:
        test["key"] = "j"

    test["presTime"] = 3000
    test["feedback"] = "incorrect: incorrect"
    test["feedbackTime"] = 1000
    test["feedbackOptions"] = "center"

    trial = pd.concat([start_trial, objects, cue, ISI, target, test], axis=0, sort=True)

    practice_trials = practice_trials.append(trial, sort=True)

subject_counter = 0

for t in range(subjectGroup):

    # THIS FUCKING TOOK FOREVER but counter balance matrix for each participant
    trial_cb = create_cb_matrix.cb_matrix_two_obj("balanceFactors_spa-att_iter05.csv")

    # initialize main dataframe
    final_trial = pd.DataFrame([])

    # initialize counters
    subject_counter += 1
    break_counter = 0
    trial_num = 0
    catch_trial_index = 0
    catch_trial_switch = 0

    for i in range(len(trial_cb)):
        # print("trial # " + str(i))
        trial_num += 1

        # START TRIAL (0/4) - ITI and alert for fixation prior to beginning of trial
        start_trial = testable.insert_essential_columns()
        start_trial["type"] = ["learn"]
        start_trial["stim1"] = fixation
        start_trial["stimFormat"] = ".png"
        start_trial["presTime"] = 700
        start_trial["ITI"] = ITI
        start_trial["stimPos"] = fovea

        # OBJECTS (1/5)
        objects = testable.insert_essential_columns()
        object_counter = trial_cb["object_pair"][i] - 1 + 10 * (trial_cb["sem_rel"][i] - 1)
        objects["type"] = ["learn"]
        objects["stim1"] = all_objects["stim1"][object_counter]
        objects["stim2"] = all_objects["stim2"][object_counter]
        objects["stim3"] = fixation
        objects["stimFormat"] = ".png"
        objects["presTime"] = object_preview_duration

        if trial_cb["stim1_loc"][i] == "left":
            objects["stimPos"] = object_left_pos + str_space + object_rght_pos + str_space + fovea

        elif trial_cb["stim1_loc"][i] == "right":
            objects["stimPos"] = object_rght_pos + str_space + object_left_pos + str_space + fovea

        # CUE (2/4)
        cue = objects.copy()
        if trial_cb["cued_object"][i] == "stim1" and trial_cb["stim1_loc"][i] == "left":
            cue["stim4"] = "cue_left"
            cue[
                "stimPos"] = object_left_pos + str_space + object_rght_pos + str_space + fovea + str_space + cue_left_pos
        elif trial_cb["cued_object"][i] == "stim1" and trial_cb["stim1_loc"][i] == "right":
            cue["stim4"] = "cue_right"
            cue[
                "stimPos"] = object_rght_pos + str_space + object_left_pos + str_space + fovea + str_space + cue_right_pos
        elif trial_cb["cued_object"][i] == "stim2" and trial_cb["stim1_loc"][i] == "left":
            cue["stim4"] = "cue_right"
            cue[
                "stimPos"] = object_left_pos + str_space + object_rght_pos + str_space + fovea + str_space + cue_right_pos
        elif trial_cb["cued_object"][i] == "stim2" and trial_cb["stim1_loc"][i] == "right":
            cue["stim4"] = "cue_left"
            cue[
                "stimPos"] = object_rght_pos + str_space + object_left_pos + str_space + fovea + str_space + cue_left_pos

        cue["presTime"] = cue_time

        # ISI (3/5)
        ISI = objects.copy()
        ISI["presTime"] = ISI_time

        # TARGET (4/5)
        target = objects.copy()
        target["stim4"] = trial_cb["target_side"][i]
        target["stim5"] = "non_target"

        if trial_cb["target_side"][i] == "target_left":
            target["stimPos"] = objects["stimPos"] + str_space + object_left_pos + str_space + object_rght_pos
        elif trial_cb["target_side"][i] == "target_right":
            target["stimPos"] = objects["stimPos"] + str_space + object_rght_pos + str_space + object_left_pos
        target["presTime"] = target_time

        # TEST (5/5)
        test = start_trial.copy()
        test["ITI"] = 0
        test["type"] = "test"
        test["keyboard"] = "f j"

        if trial_cb["target_side"][i] == "target_left":
            test["key"] = "f"
        else:
            test["key"] = "j"

        test["presTime"] = 3000
        test["feedback"] = "incorrect: incorrect"
        test["feedbackTime"] = 1000
        test["feedbackOptions"] = "center"
        test["stim1_data"] = all_objects["stim1"][object_counter]
        test["stim2_data"] = all_objects["stim2"][object_counter]
        test["trial_num"] = trial_num

        test = pd.merge(test.assign(A=1), trial_cb.iloc[[i]].assign(A=1), on="A").drop("A", 1)

        # Insert break
        if i % 30 == 0 and i > 0:
            break_counter += 1
            blocks_left = len(trial_cb) / 30 - break_counter
            blocks_left_string = str(blocks_left)

            if blocks_left_string == 1:
                break_message = "1 more block remains. Final stretch! Click below to continue"
            else:
                break_message = blocks_left_string + " blocks remaining. Take a short break. Click the button below to continue"

            rest = testable.insert_essential_columns()
            rest["type"] = ["instructions", "learn"]
            rest["content"] = [break_message, ""]
            rest["button1"] = ["Next", ""]
            rest["stim1"] = ["", "blank"]
            rest["stimFormat"] = ["", ".png"]
            rest["presTime"] = ["", "2000"]
            rest["stimPos"] = ["", "50, 50"]

            trial = pd.concat([start_trial, objects, cue, ISI, target, test, rest], axis=0, sort=True)
        else:
            trial = pd.concat([start_trial, objects, cue, ISI, target, test], axis=0, sort=True)
        final_trial = final_trial.append(trial, sort=True)

    final_trial["subjectGroup"] = subject_counter
    print("adding parNo: " + str(subject_counter))
    all_trials = all_trials.append(final_trial)

# Refine DataFrame for final trial file
all_trials = pd.concat([instructions_df, practice_trials, end_practice_break, all_trials], axis=0, sort=True)

all_trials = all_trials.set_index("subjectGroup")
all_trials = all_trials[
    ["type", "content", "trialText", "stim1", "stim2", "stim3", "stim4", "stim5", "stim6", "stimPos",
     "stimFormat", "ITI",
     "presTime", "keyboard", "key", "feedback", "feedbackTime", "feedbackOptions",
     "responseType", "responseOptions", "head", "button1",
     "block_num", "trial_num", "required", "validity", "sem_rel", "object_pair", "cued_object",
     "stim1_data", "stim2_data"]]

# Extract result to clipboard
all_trials.to_clipboard(excel=True, sep="\t")

print("done")
