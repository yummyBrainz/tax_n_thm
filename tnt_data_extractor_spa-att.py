import numpy as np
import pandas as pd
import glob, time, getpass, config
import tnt_analysis_functions as tnt_func
import matplotlib.pyplot as plt

# Data Threshold
RT_thres = 1
acc_thresh = 60
drop_rate = 10
analysis_type = 2
iteration = 4
fix_chance = 1 / 3 * 100
graph_mode = 0
obj_pairs = 1

object_preview_duration = 750

data_dir = config.exp_iteration(iteration, 750)

# Extract all data files into single np array
data_files = glob.glob(data_dir)
participants = len(data_files)
raw_data = []
for file in range(participants):
    data = pd.read_csv(data_files[file], skiprows=3)
    data["par_num"] = data_files[file][-10:-4]
    raw_data.append(data)

# concat dataframe, set index and remove practice trials
data_frame_raw = pd.concat(raw_data, sort=True)

# Divide by trial type
data_frame_raw_fix_task = data_frame_raw[data_frame_raw["trial_type"] == 2]
data_frame_raw = data_frame_raw[data_frame_raw["trial_type"] == 1]
print(data_frame_raw)
# Figure out neutral trials (1 = semantic, 2 = neutral) # iter3: (1 = object, 2 = scrambled)
data_frame_raw["obj_status"] = np.where(data_frame_raw["cued-object"] <= 2, 1, 2)

# Reset index,set par_num as index, and sort by par_num
data_frame_raw = data_frame_raw.reset_index()
data_frame_raw.set_index("par_num")
data_frame_raw = data_frame_raw.sort_values(by=["par_num", "trial_num"])

# Reset index,set par_num as index, and sort by par_num
data_frame_raw_fix_task = data_frame_raw_fix_task.reset_index()
data_frame_raw_fix_task.set_index("par_num")
data_frame_raw_fix_task = data_frame_raw_fix_task.sort_values(by=["par_num", "trial_num"])
data_frame_raw_fix_task = data_frame_raw_fix_task.rename(columns={"correct": "fix_acc"})
# Remove practice trials
data_frame_raw = data_frame_raw[data_frame_raw["subjectGroup"].notna()]

# swap thm and tax labels (incorrectly coded in counterbalance file prior to exp 3)
if iteration < 3:
    data_frame_raw = data_frame_raw.replace({"2_tax": "1_thm", "1_thm": "2_tax"})

# Drop unnecessary columns
droplist = ["index", "ITI_ms", "ITI_f", "ITI_fDuration", "feedback", "feedbackOptions", "feedbackTime",
            "head", "presTime", "presTime_f", "presTime_fDuration", "presTime_ms", "required", "responseCode",
            "responseOptions", "responseType", "timestamp", "type", "trialText"
            ]

data_frame_raw = data_frame_raw.drop(columns=droplist)

# divide up experiment into 1st and 2nd half
data_frame_raw['halves'] = data_frame_raw.apply(tnt_func.halves, axis=1)

# Remove trials based on threshold
RT_min = 150
RT_max = 2000

all_data_count = 270

# include these trials
data_frame = data_frame_raw[(data_frame_raw["RT"] >= RT_min) & (data_frame_raw["RT"] <= RT_max)]

# figure out object pairs
data_frame["obj_pair"] = data_frame["stim1"].str[:6]

# excluded trials
exclude_RT_min = data_frame_raw[(data_frame_raw["RT"] < RT_min)]
exclude_RT_max = data_frame_raw[(data_frame_raw["RT"] > RT_max)]

excluded_data_count = [exclude_RT_min.groupby(["par_num"])["RT"].count(),
                       exclude_RT_max.groupby(["par_num"])["RT"].count(),
                       all_data_count - data_frame.groupby(["par_num"])["RT"].count(),
                       (all_data_count - data_frame.groupby(["par_num"])["RT"].count()) / all_data_count * 100
                       ]
print(data_frame_raw.groupby(["par_num"])["RT"].count())
# concat trial count + % into one
excluded_data_count_df = pd.concat(excluded_data_count, axis=1, sort=True)
excluded_data_count_df.columns = ["exclude_RT_min", "exclude_RT_max", "exclude_total", "drop_rate"]

# Fill NA with 0
excluded_data_count_df = excluded_data_count_df.fillna(0)

# print(excluded_data_count_df)

# Change variable names
validity_to_words = {1: "1_valid", 2: "2_wthn", 3: "3_btwn"}
if iteration < 3:
    semantics_to_words = {1: "1_SR", 2: "2_NEU"}
else:
    semantics_to_words = {1: "1_obj", 2: "2_scr"}

data_frame.condition = data_frame.condition.replace(validity_to_words)
data_frame.obj_status = data_frame.obj_status.replace(semantics_to_words)

# New column with thematic/taxonomic/scrambled
data_frame["cued_object"] = data_frame.apply(tnt_func.sem_conds, axis=1)

# Separate out the neutral trials
data_frame["scrambled_objects"] = data_frame.apply(tnt_func.neutral, axis=1)

# # enter canonican orientation (all are hori for now)
# data_frame["canon_orient"] = data_frame.apply(tnt_func.canon_orient, axis=1)
#
# # figure out whether orientations are right
# data_frame["corr_orient"] = np.where(data_frame["canon_orient"] == data_frame["obj-orientation"], 1, 2)


# rename conditions as validity
data_frame = data_frame.rename(columns={"condition": "validity"})

# Select only neutral trials
scrambled_objects_df = data_frame[data_frame.scrambled_objects != 0]

# Now RT time
correct_trials_df = data_frame[(data_frame["correct"] == 1)]
correct_scrambled_objects_df = scrambled_objects_df[(scrambled_objects_df["correct"] == 1)]

# Calculate overall accuracy
accuracy_df = data_frame.groupby(["par_num"])["correct"].mean() * 100

# Calculate accuracy by conditions
accuracy_allcond_df = data_frame.groupby(["par_num", "validity", "cued_object"])["correct"].mean().unstack(
    ["validity", "cued_object"]) * 100
accuracy_allcond_df.columns = ["acc_valid_thm", "acc_valid_tax", "acc_valid_neu",
                               "acc_wthn_thm", "acc_wthn_tax", "acc_wthn_neu",
                               "acc_btwn_thm", "acc_btwn_tax", "acc_btwn_neu"]

# Calculate RT by conditions
RT_allcond_df = correct_trials_df.groupby(["par_num", "validity", "cued_object"])["RT"].mean().unstack(
    ["validity", "cued_object"])
RT_allcond_df.columns = ["RT_valid_thm", "RT_valid_tax", "RT_valid_neu",
                         "RT_wthn_thm", "RT_wthn_tax", "RT_wthn_neu",
                         "RT_btwn_thm", "RT_btwn_tax", "RT_btwn_neu"]
# Divide up the neutral
RT_neutral = correct_scrambled_objects_df.groupby(["par_num", "validity", "scrambled_objects"])["RT"].mean().unstack(
    ["validity", "scrambled_objects"])
RT_neutral.columns = ["RT_valid_scrmb(thm)", "RT_valid_scrmb(tax)",
                      "RT_wthn_scrmb(thm)", "RT_wthn_scrmb(tax)",
                      "RT_btwn_scrmb(thm)", "RT_btwn_scrmb(tax)",
                      ]

acc_neutral = scrambled_objects_df.groupby(["par_num", "validity", "scrambled_objects"])["correct"].mean().unstack(
    ["validity", "scrambled_objects"]) * 100
acc_neutral.columns = ["acc_valid_scrmb(thm)", "acc_valid_scrmb(tax)",
                       "acc_wthn_scrmb(thm)", "acc_wthn_scrmb(tax)",
                       "acc_btwn_scrmb(thm)", "acc_btwn_scrmb(tax)",
                       ]

RT_objpair_df = correct_trials_df.groupby(["par_num", "obj_pair", "validity"])["RT"].mean().unstack(
    ["obj_pair", "validity"])

RT_objpair_count_df = correct_trials_df.groupby(["par_num", "obj_pair", "validity"])["RT"].count().unstack(
    ["obj_pair", "validity"])

RT_halves_allcond_df = correct_trials_df.groupby(["par_num", "halves", "validity", "cued_object"])["RT"].mean().unstack(
    ["halves", "validity", "cued_object"])
acc_halves_allcond_df = data_frame.groupby(["par_num", "halves", "validity", "obj_status"])["correct"].mean().unstack(
    ["halves", "validity", "obj_status"]) * 100

fix_task_data = [data_frame_raw_fix_task.groupby(["par_num"])["fix_acc"].mean() * 100,
                 data_frame_raw_fix_task.groupby(["par_num"])["fix_acc"].sum()
                 ]
fix_task_df = pd.concat(fix_task_data, axis=1, sort=True)
fix_task_df.columns = ["fix_acc", "fix_corr_trials"]

# data_frame_raw_fix

# misc information
misc_df = data_frame.groupby(["par_num"])["subjectGroup"].mean()

# mjolnir = pd.concat([fix_task_df, misc_df, excluded_data_count_df, accuracy_df, accuracy_allcond_df, RT_allcond_df, RT_neutral, acc_neutral, RT_halves_allcond_df], axis=1, sort=True)
mjolnir = pd.concat([excluded_data_count_df, accuracy_df, RT_objpair_df], axis=1, sort=True)
stormbreaker = pd.concat([excluded_data_count_df, accuracy_df, RT_objpair_count_df], axis=1, sort=True)
# mjolnir = pd.concat([excluded_data_count_df, accuracy_df], axis=1, sort=True)

# Whoever shall be worthy may wield the power of mjolnir
worthy = mjolnir.index[(mjolnir["correct"] >= acc_thresh) & (mjolnir["drop_rate"] < drop_rate)]
worthy2 = stormbreaker.index[(stormbreaker["correct"] >= acc_thresh) & (stormbreaker["drop_rate"] < drop_rate)]

mjolnir = mjolnir.loc[worthy]
stormbreaker = stormbreaker.loc[worthy]
# mjolnir.to_clipboard(excel=True, sep="\t")

if obj_pairs == 1:
    mjolnir = mjolnir.drop(columns=["exclude_RT_min", "exclude_RT_max", "exclude_total", "drop_rate", "correct"])
    stormbreaker = stormbreaker.drop(columns=["exclude_RT_min", "exclude_RT_max", "exclude_total", "drop_rate", "correct"])

    mjolnir = mjolnir.reindex(sorted(mjolnir.columns), axis=1)
    stormbreaker = stormbreaker.reindex(sorted(stormbreaker.columns), axis=1)

    neu_pairs = mjolnir.iloc[:, 0:30]
    tax_pairs = mjolnir.iloc[:, 30:60]
    thm_pairs = mjolnir.iloc[:, 60:90]

    neu_pairs.mean().plot.bar(color=["red", "blue", "purple"], width=0.5)
    plt.show()
    plt.savefig('fig2_bar.png', format="png")


    tax_pairs.mean().plot.bar(color=["red", "blue", "purple"])
    plt.show()

    thm_pairs.mean().plot.bar(color=["red", "blue", "purple"])
    plt.show()

print("Data ready for analysis")

if graph_mode == 1:
    print("Now printing graphs...")
